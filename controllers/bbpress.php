<?php
/*
Controller name: bbPress
Controller description: bbPress RESTful API methods
*/

class JSON_API_BBPress_Controller
{
	public function get_forums() {

		$forums = new \_03d64f545b651804f6b99f1369d38fa6\Controller\GetForumsController();
		$results = $forums->getAction();

		return $results;
	}

}
