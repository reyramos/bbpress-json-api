<?php
/*
Plugin Name: bbPress JSON API extension
Plugin URI:
Description: An extension to JSON API RESTful API and bbPress to display Forum content to RESTful API
Version: 0.0.1
Author:
Author URI: Rey Ramos
*/

if(!defined ( 'ENVIROMENT')){
	define( 'ENVIROMENT', (isset($_SERVER['APPLICATION_ENV']) && $_SERVER['APPLICATION_ENV'] == 'development')?'development':'production');
}

if (ENVIROMENT == 'development') {
	error_reporting(E_ALL);
	ini_set("display_errors", 1);
}

include_once(ABSPATH . 'wp-admin/includes/plugin.php');

define('BBPRESS_JSON_API_PLUGIN_MD5', '_03d64f545b651804f6b99f1369d38fa6');
define('BBPRESS_JSON_API_PLUGIN_PATH', plugin_dir_path(__FILE__));
define('BBPRESS_JSON_API_PLUGIN_URL', plugins_url("bbpress-json-api"));

include_once BBPRESS_JSON_API_PLUGIN_PATH . 'vendor/autoload.php';

if (!is_plugin_active('bbpress/bbpress.php') || !is_plugin_active('json-api/json-api.php')) {
	add_action('admin_notices', '_03d64f545b651804f6b99f1369d38fa6_draw_notice');
	return;
}

add_filter('json_api_controllers', '_03d64f545b651804f6b99f1369d38fa6_bbPress_controller');

function _03d64f545b651804f6b99f1369d38fa6_bbPress_controller($controllers)
{
	$controllers[] = 'BBPress';
	return $controllers;
}


add_filter('json_api_bbpress_controller_path', '_03d64f545b651804f6b99f1369d38fa6_bbPress_controller_path');
function _03d64f545b651804f6b99f1369d38fa6_bbPress_controller_path()
{
	return BBPRESS_JSON_API_PLUGIN_PATH . 'controllers/bbpress.php';
}


function _03d64f545b651804f6b99f1369d38fa6_draw_notice()
{
	if (!is_plugin_active('bbpress/bbpress.php')) {
		echo '<div id="JSON_message" class="error fade"><p style="line-height: 150%">';
		_e('<strong>bbPress JSON API extension</strong> requires the bbPress plugin to be activated. Please <a href="http://wordpress.org/plugins/bbpress/">install / activate bbPress</a> first,
	or <a href="plugins.php">deactivate bbPress JSON API extension</a>.');
		echo '</p></div>';
	}

	if (!is_plugin_active('json-api/json-api.php')) {
		echo '<div id="JSON_message" class="error fade"><p style="line-height: 150%">';
		_e('<strong>bbPress JSON API extension</strong> requires the bbPress plugin to be activated. Please <a href="http://wordpress.org/plugins/bbpress/">install / activate bbPress</a> first,
	or <a href="plugins.php">deactivate bbPress JSON API extension</a>.');
		echo '</p></div>';
	}

	_03d64f545b651804f6b99f1369d38fa6_set_inactive();
}


/**
 * Hack to modify the view of the plugin page if plugin is not activated
 */
function _03d64f545b651804f6b99f1369d38fa6_set_inactive()
{
	echo '<script>
			window.onload = function (){
				document.getElementById("bbpress-json-api-extension").classList.remove ("active");
				document.getElementById("bbpress-json-api-extension").classList.add ("inactive");
			}
		</script>';

	_03d64f545b651804f6b99f1369d38fa6_destroy_activation();
	add_filter('plugin_action_links_' . plugin_basename(__FILE__), '_03d64f545b651804f6b99f1369d38fa6_set_action_buttons', 10, 2);
}


function _03d64f545b651804f6b99f1369d38fa6_set_action_buttons($links)
{
	unset($links['deactivate']);

	$mylinks = array(
		'activate' => 'Activate',
		'edit' => 'Edit',
		'z' => 'Delete'
	);

	$newArr = array_merge($links, $mylinks);
	ksort($newArr);
	return $newArr;
}

/**
 * Update option_value from set active_plugins
 */
function _03d64f545b651804f6b99f1369d38fa6_destroy_activation()
{
	global $wpdb;
	//first check if it already exist
	$result = $wpdb->get_row("SELECT option_value FROM " . $wpdb->prefix . "options  WHERE option_name = 'active_plugins'");
	$unserialize = unserialize($result->option_value);
	foreach ($unserialize as $key => $string) {
		if (strcasecmp($string, trim(plugin_basename(__FILE__))) == 0) {
			$index = $key;
		}
	}
	if (isset($index)) {
		unset($unserialize[$index]);
		$serialize = serialize($unserialize);
		$wpdb->update($wpdb->prefix . "options", array('option_value' => $serialize), array('option_name' => 'active_plugins'), array('%s'));
	}
}