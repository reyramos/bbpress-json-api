<?php
/**
 * Created by PhpStorm.
 * User: Reymundo.Ramos
 * Date: 7/2/14
 * Time: 11:17 AM
 */
namespace _03d64f545b651804f6b99f1369d38fa6\Controller;


class GetForumsController
{

	private $post_count = 0;

	public function __construct(){}

	public function getAction()
	{
		$results = array();
		$type = 'forum';
		$args = array(
			'post_type' => $type,
			'post_status' => 'publish',
			'posts_per_page' => -1,
			'order' => 'ASC'
		);

		$my_query = null;
		$my_query = new \WP_Query($args);
		if ($my_query->have_posts()) {

			$json_array = json_decode(json_encode($my_query->posts), true);
			$results = array_merge(array('status' => 'ok', 'count' => sizeof($json_array)));
			$forums = array();
			foreach ($json_array as $key => $arr) {
				$this->post_count = 0;
				$handler = $this->_setHandlerKeys(null, $arr);
				$handler['topic_count'] = 0;
				$forums[$key] = $handler;
				$this->_getTopic($forums[$key], $arr['ID']);
				$forums[$key]['post_count'] = $this->post_count;
			}

			$results = array_merge($results, array('forums' => $forums));
		}

		if (isset($GLOBALS['wp_the_query']))
			wp_reset_query();

		return $results;
	}

	private function _setHandlerKeys($key = null, $array)
	{
		$handler = isset($key) && $key === null ? $handler[$key] = array() : array();
		$handler['id'] = $array['ID'];
		$handler['type'] = $array['post_type'];
		$handler['slug'] = $array['post_name'];
		$handler['url'] = $array['guid'];
		$handler['status'] = $array['post_status'];
		$handler['title'] = $array['post_title'];
		$handler['content'] = $array['post_content'];
		$handler['excerpt'] = $array['post_excerpt'];
		$handler['date'] = $array['post_date'];
		$handler['modified'] = $array['post_modified'];

		$user = new \WP_User($array['post_author']);
		$user_info = json_decode(json_encode($user->data), true);

		$handler['author']['id'] = $user_info['ID'];
		$handler['author']['slug'] = $user_info['ID'];
		$handler['author']['display_name'] = $user_info['display_name'];
		$handler['author']['email'] = $user_info['user_email'];
		$handler['author']['first_name'] = "";
		$handler['author']['last_name'] = "";
		$handler['author']['url'] = $user_info['user_url'];

		return $handler;
	}

	private function _getTopic(&$handler, $post_parent)
	{
		$handler['topics'] = array();
		$type = 'topic';
		$args = array(
			'post_type' => $type,
			'post_status' => 'publish',
			'post_parent__in' => array($post_parent),
			'posts_per_page' => -1,
			'order' => 'DESC'
		);


		$my_query = null;
		$my_query = new \WP_Query($args);
		if ($my_query->have_posts()) {
			$json_array = json_decode(json_encode($my_query->posts), true);
			$handler['topic_count'] = sizeof($json_array);

			foreach ($json_array as $key => $arr) {
				$temp = $this->_setHandlerKeys('topics', $arr);
				$handler['topics'][$key] = $temp;
				$handler['topics'][$key]['reply_count'] = 1; //always count itself
				$this->_getReply($handler['topics'][$key], $arr['ID']);
				$this->post_count += $handler['topics'][$key]['reply_count'];
			}

		}
	}

	private function _getReply(&$handler, $post_parent)
	{
		$handler_copy = $handler;
		$handler['reply'] = array();

		$handler_copy['title'] = "";
		unset($handler_copy['reply_count']);
		unset($handler_copy['reply']);

		$handler['reply'][0] = $handler_copy;

		$type = 'reply';
		$args = array(
			'post_type' => $type,
			'post_status' => 'publish',
			'post_parent__in' => array($post_parent),
			'posts_per_page' => -1,
			'order' => 'ASC'
		);

		$my_query = null;
		$my_query = new \WP_Query($args);
		if ($my_query->have_posts()) {
			$json_array = json_decode(json_encode($my_query->posts), true);
			$handler['reply_count'] = sizeof($json_array) + 1;

			foreach ($json_array as $key => $arr) {
				$reply = $this->_setHandlerKeys('reply', $arr);
				$handler['reply'][$key + 1] = $reply;
			}
		}
	}
} 